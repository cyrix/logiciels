# Logiciels

## Audio
- [Lollypop](https://wiki.gnome.org/Apps/Lollypop) : lecteur/gestionnaire de musique, GPLv3
- [Tauon Music Box](https://tauonmusicbox.rocks/) : autre lecteur/gestionnaire de musique, GPLv3
- [Flacon](https://flacon.github.io/) : extrait les pistes d'une image de CD audio en FLAC (ou autre format), LGPLv2.1
- [Asunder](http://littlesvr.ca/asunder/index.php) : extrait les pistes d'un CD en FLAC (ou autre format), GPLv2
- [puddletag](https://docs.puddletag.net/index.html) : édite les tags de fichiers audio, GPLv3
- [fre:ac (free audio converter)](https://www.freac.org/) : convertisseur de plusieurs formats de fichiers audio, GPLv2
- [PulseAudio Preferences (paprefs)](https://freedesktop.org/software/pulseaudio/paprefs/) : simple interface de configuration du serveur de son PulseAudio, GPLv2
- [EasyEffects](https://github.com/wwmm/easyeffects) (memo) : Effets audio pour logiciels fonctionnant avec PipeWire, GPLv3
- [JamesDSP](https://github.com/Audio4Linux/JDSP4Linux) (memo) : Effets audio pour logiciels fonctionnant avec PipeWire ou PulseAudio, GPLv3
- [NoiseTorch](https://github.com/lawl/NoiseTorch) (memo) : réduction du bruit du micro en temps réel pour PulseAudio/PipeWire, GPLv3
- [Mixxx](https://mixxx.org/) : alternative libre de VirtualDJ, GPLv2

## Communication
- [Ricochet Refresh](https://www.ricochetrefresh.net/) (memo) : logiciel de chat instantané utilisant Tor, BSD-3-Clause
- [qTox](https://qtox.github.io/) : logiciel de chat instantané, conférences audio/vidéo et transfert de fichiers utilisant le protocole P2P Tox, GPLv3

## Vidéo
- [Cinelerra GG Infinity](https://www.cinelerra-gg.org/) (memo) : logiciel de montage, GPL
- [Natron](https://natrongithub.github.io/) (memo) : Compositing Software For VFX and Motion Graphics, GPLv2
- [vokoscreenNG](https://github.com/vkohaupt/vokoscreenNG) (memo) : logiciel de capture d'écran/audio, GPLv2

## Depuis le terminal
- [micro](https://micro-editor.github.io/index.html) : éditeur de texte, MIT
- [streamlink](https://github.com/streamlink/streamlink) : permet de récupérer le flux d'un direct et l'ouvrir dans notre lecteur favori, BSD-2-Clause
- [YT-DLP](https://github.com/yt-dlp/yt-dlp) : permet de télécharger des videos depuis Youtube ou d'autres sites (fork de [youtube-dl](https://github.com/ytdl-org/youtube-dl)), domaine public
- [campdown](https://github.com/catlinman/campdown/) : permet de télécharger la musique disponible sur [Bandcamp](https://bandcamp.com/), MIT
- [khinsider](https://github.com/obskyr/khinsider) : permet de télécharger les bandes-son de jeux vidéo disponibles sur [https://downloads.khinsider.com/](https://downloads.khinsider.com/)
- [mat2](https://0xacab.org/jvoisin/mat2) : permet de supprimer les métadonnées de fichiers, LGPLv3
- [dnuos](https://bitheap.org/dnuos/) : créateur de liste de collection de musique, GPL
- [btop++](https://github.com/aristocratos/btop) : moniteur de ressources (CPU, RAM, disques, réseau, processus), version C++ de [bpytop](https://github.com/aristocratos/bpytop), Apache-2.0
- [ncdu](https://dev.yorhel.nl/ncdu) : analyseur d'espace disque pour visualiser rapidement ce qui occupe ce dernier, MIT
- [Magnet2Torrent](https://github.com/LordAro/Magnet2Torrent) /!\ a besoin d'être patché /!\ : convertit un lien magnet en .torrent, GPLv3
- [torf-cli](https://github.com/rndusr/torf-cli) : outil pour créer/lire/éditer des .torrent, GPLv3
- [gendesk](https://github.com/xyproto/gendesk) (memo) : Generate .desktop files and download .png icons by specifying a minimum of information, MIT
- [innoextract](https://constexpr.org/innoextract/) : outil pour extraire les installeurs créés avec Inno Setup (les jeux GOG par exemple), Zlib

## Outils divers
- [Barrier](https://github.com/debauchee/barrier) : permet de contrôler plusieurs PC avec un seul kit clavier/souris, GPLv2
- [Nyrna](https://nyrna.merritt.codes/) : permet de suspendre des applications pour libérer l'usage du CPU/GPU, GPLv3
- [pdftag](https://github.com/arrufat/pdftag) : éditeur de métadonnées de PDF, GPLv3
- [libstrangle](https://gitlab.com/torkel104/libstrangle) : limiteur de FPS pour jeux tournant sous OpenGL, GPLv3
- [ReplaySorcery](https://github.com/matanui159/ReplaySorcery) : "instant-replay solution" capture les 30 dernières secondes de son écran, GPLv3
- [rofi](https://github.com/davatorium/rofi) (memo) : A window switcher, application launcher and dmenu replacement, MIT
- [Grisbi](https://grisbi.org/) : logiciel de comptabilité, GPLv2
- [WoeUSB](https://github.com/WoeUSB/WoeUSB) : outil pour créer une clé d'installation Windows depuis Linux, GPLv3
- [scrcpy](https://github.com/Genymobile/scrcpy) : permet d'afficher/contrôler un appareil Android≥5.0 branché en USB, Apache-2.0
- [Quickemu](https://github.com/wimpysworld/quickemu) : script permettant de créer rapidement des machines virtuelles via quemu, MIT
- [Quickgui](https://github.com/quickgui/quickgui) : interface graphique pour Quickemu, pas encore de licence
- [linux-autoscroll](https://github.com/TWolczanski/linux-autoscroll) : script permettant la fonction autoscroll similaire à Windows avec le clic molette (ou autre touche), pas de licence
- [XClicker](https://xclicker.pages.dev/) : autoclicker pour Linux, GPLv3
- [OpenRGB](https://openrgb.org/) : permet de contrôler les lumières RGB de plusieurs composants/périphériques, GPLv2

## Réseau
- [Angry IP Scanner](https://angryip.org/) : scanneur de réseau, GPLv2
- [Tremotesf](https://github.com/equeim/tremotesf2) : Remote GUI pour transmission-daemon, GPLv3
- [OpenSnitch](https://github.com/evilsocket/opensnitch) : application de pare-feu, portage de Little Snitch (macOS), GPLv3
- [Lagrange](https://git.skyjake.fi/gemini/lagrange) : navigateur/client pour Gemini (nouveau protocole web remplaçant HTTP/HTTPS), BSD-2-Clause
- [speedtest-cli](https://github.com/sivel/speedtest-cli) : outil de test de bande passante via speedtest.net, Apache-2.0

## Système
- [Yup](https://github.com/ericm/yup) : gestionnaire de paquets AUR, GPLv3
- [expac](https://github.com/falconindy/expac) : outil d'extraction de base de données pacman, GPL
- [Cockpit](https://cockpit-project.org/) : interface web d'administration de serveurs, LGPLv2.1
- [WinApps](https://github.com/Fmstrat/winapps) (memo) : script permettant d'intégrer des logiciels Windows à notre système comme si c'était des logiciels Linux natifs, pas de licence
- [Scaphandre](https://github.com/hubblo-org/scaphandre) : outil pour mesurer la consommation électrique de notre machine, Apache-2.0

## Sécurité
- [KeePassXC](https://keepassxc.org/) : gestionnaire de mots de passe, GPLv2/3

## Périphériques
- [ckb-next](https://github.com/ckb-next/ckb-next) : pilote open-source pour claviers et souris Corsair, GPLv2
- [NumLockX](https://github.com/rg3/numlockx) : permet d'activer le pavé numérique au démarrage, MIT
- [Oversteer](https://github.com/berarma/oversteer) : gestionnaire de volants pour Linux, GPLv3


## Jeux vidéo
### Ports / Réimplémentations de moteur (nécessite les données du jeu original)
- [DevilutionX](https://github.com/diasurgical/devilutionX) : pour **Diablo**, domaine public
- [Julius](https://github.com/bvschaik/julius) : pour **Caesar III** en conservant le gameplay d'origine, AGPLv3
- [Augustus](https://github.com/Keriew/augustus) : pour **Caesar III** qui ajoute des améliorations de gameplay, AGPLv3
- [fheroes2](https://ihhub.github.io/fheroes2/) : pour **Heroes Of Might And Magic II**, GPLv2
- [VCMI](https://github.com/vcmi/vcmi) : pour **Heroes Of Might And Magic III**, GPLv2
- [OpenMW](https://gitlab.com/OpenMW/openmw) : pour **The Elder Scrolls III: Morrowind**, GPLv3
- [R.E.L.I.V.E.](https://aliveteam.github.io/index.html) : pour **Oddworld : L'Odyssée d'Abe** et **Oddworld : L'Exode d'Abe**, pas encore de licence pour l'instant mais à l'avenir probablement sous MIT
- [FreeSynd](https://freesynd.sourceforge.io/) : pour **Syndicate**, GPLv2
- [SpaceCadetPinball](https://github.com/k4zmu2a/SpaceCadetPinball) : port de **3D Pinball for Windows – Space Cadet**, MIT
- [Doom64 EX](https://github.com/svkaiser/Doom64EX) : port PC de **DOOM 64**, GPLv2
- [OpenXRay](https://github.com/OpenXRay/xray-16) : version améliorée, suite à la fuite du code source, du X-Ray Engine pour les jeux **S.T.A.L.K.E.R.**, [zone grise](https://github.com/OpenXRay/xray-16/blob/dev/License.txt) : techniquement appartient toujours à GSC Game World
- [Arx Libertatis](https://arx-libertatis.org/) : pour **Arx Fatalis**, GPLv3
- [REDRIVER2](https://github.com/OpenDriver2/REDRIVER2) : port PC reverse-engineered de **Driver 2** (PS1) pour Linux/Windows, MIT

### Outils divers
- [jPSXdec](https://github.com/m35/jpsxdec) : permet d'extraire l'audio/vidéo de CD PS1, gratuit pour usage non-commercial
- [apascan](https://github.com/chewi/apascan) : scanneur de partitions de HDD de PS2 pour Linux, GPLv2
- [psxrip](https://github.com/hydrian/psxrip) : script bash pour sauvegarder des CD PS1 au format .bin/.cue, GPLv3
- [vkBasalt](https://github.com/DadSchoorse/vkBasalt) : outil basé sur Vulkan pour améliorer les graphismes en jeu, Zlib
- [MangoHud](https://github.com/flightlessmango/MangoHud) : Overlay Vulkan/OpenGL affichant FPS, températures/charges du CPU/GPU etc, MIT
- [GOverlay](https://github.com/benjamimgois/goverlay) : GUI pour gérer plusieurs overlays dont vkBasalt/MangoHud/ReplaySorcery, GPLv3
- [SLADE3](https://slade.mancubus.net/index.php?page=about) : éditeur de **DOOM**, GPLv2
- [AntiMicroX](https://github.com/AntiMicroX/antimicrox) : logiciel pour mapper des touches du clavier et la souris vers une manette, GPLv3
- [aoe2techtree](https://aoe2techtree.net/?lng=fr) : version web de l'arbre des technologies d'**Age of Empires II: Definitive Edition**, MIT
- [mymc+](https://git.sr.ht/~thestr4ng3r/mymcplus) : gestionnaire de cartes mémoires de PS2, GPLv3
- [Chiaki](https://git.sr.ht/~thestr4ng3r/chiaki) : client Linux (et autres plateformes) pour Playstation 4/5 Remote Play, AGPLv3

